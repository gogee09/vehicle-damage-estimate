from typing import Optional
from django.contrib.auth import authenticate, login
from strawberry.types import Info

from .queries import User

import strawberry

import strawberry_django_jwt.mutations as jwt_mutations


@strawberry.input
class UserAuthenticateInput:
    username: Optional[str]
    password: Optional[str]


@strawberry.type
class UserAuthenticateMutation:
    token_auth: str = jwt_mutations.ObtainJSONWebToken.obtain
    verify_token: str = jwt_mutations.Verify.verify
    refresh_token: str = jwt_mutations.Refresh.refresh
    delete_token_cookie: str = jwt_mutations.DeleteJSONWebTokenCookie.delete_cookie

    @strawberry.mutation
    def authenticate_user(self, input: UserAuthenticateInput, info: Info) -> User:
        user = authenticate(username=input.username, password=input.password)
        if user is not None:
            login(info.context.request, user)
            return user
