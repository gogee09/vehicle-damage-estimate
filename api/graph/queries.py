from typing import Optional
from django.contrib.auth import models
from strawberry.types import Info

import datetime
import strawberry


@strawberry.type
class User:
    id: strawberry.ID
    username: Optional[str]
    first_name: Optional[str]
    last_name: Optional[str]
    email: Optional[str]
    password: Optional[str]
    is_staff: Optional[bool]
    is_active: Optional[bool]
    is_superuser: Optional[bool]
    last_login: Optional[datetime.date]
    date_joined: Optional[datetime.date]

    @strawberry.field
    def session_id(self, info: Info) -> Optional[str]:
        return info.context.request.session.session_key


@strawberry.type
class UserQuery:

    @strawberry.field
    def me(self, info: Info) -> User:
        return models.User.objects.get(username=info.context.request.user.username)