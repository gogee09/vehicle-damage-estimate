import typing

from strawberry.permission import BasePermission
from strawberry.types import Info


def PermissionPerModel(permission_name: "IsAuthorized") -> "IsAuthorized":
    class IsAuthorized(BasePermission):
        message = "You don't have enough permissions"

        def has_permission(self, source: typing.Any, info: Info, **kwargs) -> bool:
            user = info.context.request.user
            if user.is_authenticated and user.is_staff or user.is_superuser:
                if user.has_perm(permission_name):
                    return True

    return IsAuthorized
