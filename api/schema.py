import strawberry

from strawberry_django_jwt.middleware import JSONWebTokenMiddleware

from car_damage_estimate.schema.queries import CarDamageEstimateQuery
from car_damage_estimate.schema.mutations import CarDamageEstimateMutation
from car_damage_estimate.schema.filters import CarDamageEstimateFilterMutation
from .graph import queries
from .graph import mutations


@strawberry.type
class Query:
    car_damage_estimates: CarDamageEstimateQuery = strawberry.field(resolver=lambda: CarDamageEstimateQuery)
    user: queries.UserQuery = strawberry.field(resolver=lambda: queries.UserQuery)


@strawberry.type
class Mutation:
    car_damage_estimates: CarDamageEstimateMutation = strawberry.field(resolver=lambda: CarDamageEstimateMutation)
    car_damage_estimate_filters: CarDamageEstimateFilterMutation = strawberry.field(resolver=lambda: CarDamageEstimateFilterMutation)
    user_auth: mutations.UserAuthenticateMutation = strawberry.field(
        resolver=lambda: mutations.UserAuthenticateMutation)


schema = strawberry.Schema(
    query=Query, mutation=Mutation,
    extensions=[
        JSONWebTokenMiddleware]
)
