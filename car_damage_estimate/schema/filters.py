from typing import Optional

from django.db.models import Q
import strawberry
from car_damage_estimate import models

from car_damage_estimate.schema.queries import CounterAgent


@strawberry.input
class SearchCounterAgentInput:
    search_term: Optional[str]


@strawberry.input
class SearchCounterAgentByNameInput:
    search_list: Optional[list[str]]


@strawberry.type
class CarDamageEstimateFilterMutation:
    @strawberry.mutation
    def search_counteragents(self, input: SearchCounterAgentInput) -> list[CounterAgent]:
        return models.CounterAgent.objects.filter(Q(name__icontains=input.search_term) |
                                                  Q(type__icontains=input.search_term) |
                                                  Q(address__icontains=input.search_term) |
                                                  Q(phone_number__icontains=input.search_term) |
                                                  Q(payment_account__icontains=input.search_term))

    @strawberry.mutation
    def search_counteragent_by_name(self, input: SearchCounterAgentByNameInput) -> list[CounterAgent]:
        return models.CounterAgent.objects.filter(name__in=input.search_list)