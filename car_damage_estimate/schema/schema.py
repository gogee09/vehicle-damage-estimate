import strawberry

from car_damage_estimate.schema.queries import CarDamageEstimateQuery
from car_damage_estimate.schema.mutations import CarDamageEstimateMutation

schema = strawberry.Schema(
    query=CarDamageEstimateQuery, mutation=CarDamageEstimateMutation
)
