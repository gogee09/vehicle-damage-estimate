from typing import Optional

from api.permissions import PermissionPerModel
from car_damage_estimate import models
from .permissions import Permissions

import strawberry
import datetime


@strawberry.type
class CounterAgent:
    id: strawberry.ID
    name: Optional[str]
    type: Optional[str]
    address: Optional[str]
    phone_number: Optional[str]
    payment_account: Optional[str]


@strawberry.type
class Vehicle:
    id: strawberry.ID
    vehicle_mark: Optional[str]
    vehicle_reg_number: Optional[str]
    color: Optional[str]
    mileage: Optional[int]
    year_of_production: Optional[datetime.date]
    serial_and_pts: Optional[str]
    vin_number: Optional[str]
    engine_serial_number: Optional[int]
    chassis_serial_number: Optional[int]
    owner: Optional[CounterAgent]


@strawberry.type
class Employee:
    id: strawberry.ID
    birth_date: Optional[datetime.date]
    passport_serial: Optional[str]
    address: Optional[str]
    phone_number: Optional[str]
    fms: Optional[str]


@strawberry.type
class VehicleDamageEstimate:
    id: strawberry.ID
    estimation_date: Optional[datetime.date]
    case_number: Optional[str]
    order_number: Optional[int]
    vehicle_id: Optional[Vehicle]
    registration_number: Optional[int]
    vehicle_owner: Optional[CounterAgent]
    estimator: Optional[Employee]


@strawberry.type
class BaseMaterialModel:
    id: strawberry.ID
    name: Optional[str]
    measure_name: Optional[str]
    quantity: Optional[int]
    cost: Optional[float]


@strawberry.type
class Material(BaseMaterialModel):
    material_name: Optional[str]


@strawberry.type
class SparePart(BaseMaterialModel):
    spare_part_name: Optional[str]


@strawberry.type
class Work(BaseMaterialModel):
    work_name: Optional[str]
    norma_hour: Optional[float]


@strawberry.type
class CarDamageEstimateQuery:
    permissions: Permissions = strawberry.field(resolver=lambda: True)
    counter_agents: list[CounterAgent] = strawberry.field(
        resolver=lambda: models.CounterAgent.objects.all(),
        permission_classes=[
            PermissionPerModel("car_damage_estimate.view_counteragent"),
        ],
    )
    vehicles: list[Vehicle] = strawberry.field(
        resolver=lambda: models.Vehicle.objects.all(),
        permission_classes=[PermissionPerModel("car_damage_estimate.view_vehicle")],
    )
    employees: list[Employee] = strawberry.field(
        resolver=lambda: models.Employee.objects.all(),
        permission_classes=[PermissionPerModel("car_damage_estimate.view_employee")],
    )
    vehicle_damage_estimates: list[VehicleDamageEstimate] = strawberry.field(
        resolver=lambda: models.VehicleDamageEstimate.objects.all(),
        permission_classes=[
            PermissionPerModel("car_damage_estimate.view_vehicledamageestimate")
        ],
    )
    materials: list[Material] = strawberry.field(
        resolver=lambda: models.Material.objects.all(),
        permission_classes=[PermissionPerModel("car_damage_estimate.view_material")],
    )
    works: list[Work] = strawberry.field(
        resolver=lambda: models.Work.objects.all(),
        permission_classes=[PermissionPerModel("car_damage_estimate.view_work")],
    )
    spare_parts: list[SparePart] = strawberry.field(
        resolver=lambda: models.SparePart.objects.all(),
        permission_classes=[PermissionPerModel("car_damage_estimate.view_sparepart")],
    )

    @strawberry.field(
        permission_classes=[PermissionPerModel("car_damage_estimate.view_counteragent")]
    )
    def counter_agent(
        self, counter_agent_id: list[strawberry.ID]
    ) -> list[CounterAgent]:
        return models.CounterAgent.objects.filter(id__in=counter_agent_id)

    @strawberry.field(
        permission_classes=[PermissionPerModel("car_damage_estimate.view_vehicle")]
    )
    def vehicle(self, vehicle_id: list[strawberry.ID]) -> list[Vehicle]:
        return models.Vehicle.objects.filter(id__in=vehicle_id)

    @strawberry.field(
        permission_classes=[PermissionPerModel("car_damage_estimate.view_employee")]
    )
    def employee(self, employee_id: list[strawberry.ID]) -> list[Employee]:
        return models.Employee.objects.filter(id__in=employee_id)

    @strawberry.field(
        permission_classes=[
            PermissionPerModel("car_damage_estimate.view_vehicledamageestimate")
        ]
    )
    def vehicle_damage_estimate(
        self, vehicle_damage_estimate_id: list[strawberry.ID]
    ) -> list[VehicleDamageEstimate]:
        return models.VehicleDamageEstimate.objects.filter(
            id__in=vehicle_damage_estimate_id
        )

    @strawberry.field(
        permission_classes=[PermissionPerModel("car_damage_estimate.view_material")]
    )
    def material(self, material_id: list[strawberry.ID]) -> list[Material]:
        return models.Material.objects.filter(id__in=material_id)

    @strawberry.field(
        permission_classes=[PermissionPerModel("car_damage_estimate.view_work")]
    )
    def work(self, work_id: list[strawberry.ID]) -> list[Work]:
        return models.Work.objects.filter(id__in=work_id)

    @strawberry.field(
        permission_classes=[PermissionPerModel("car_damage_estimate.view_sparepart")]
    )
    def spare_part(self, spare_part_id: list[strawberry.ID]) -> list[SparePart]:
        return models.SparePart.objects.filter(id__in=spare_part_id)