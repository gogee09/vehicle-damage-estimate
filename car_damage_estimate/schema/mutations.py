import datetime
from typing import Optional
from car_damage_estimate import models
import strawberry
from api.permissions import PermissionPerModel

from car_damage_estimate.schema.queries import (
    Vehicle,
    Employee,
    CounterAgent,
    VehicleDamageEstimate,
    Material,
    SparePart,
    Work,
)


@strawberry.input
class CounterAgentInput:
    name: Optional[str] = ""
    type: Optional[str] = ""
    address: Optional[str] = ""
    phone_number: Optional[str] = ""
    payment_account: Optional[str] = ""


@strawberry.input
class VehicleInput:
    vehicle_mark: Optional[str] = ""
    vehicle_reg_number: Optional[str] = ""
    color: Optional[str] = ""
    mileage: Optional[int] = None
    year_of_production: Optional[datetime.date] = None
    serial_and_pts: Optional[str] = ""
    vin_number: Optional[str] = ""
    engine_serial_number: Optional[int] = None
    chassis_serial_number: Optional[int] = None
    owner_id: Optional[strawberry.ID] = ""


@strawberry.input
class EmployeeInput:
    birth_date: Optional[datetime.date] = None
    passport_serial: Optional[str] = ""
    address: Optional[str] = ""
    phone_number: Optional[str] = ""
    fms: Optional[str] = ""


@strawberry.input
class VehicleDamageEstimateInput:
    estimation_date: Optional[datetime.date] = None
    case_number: Optional[str] = ""
    order_number: Optional[int] = None
    vehicle_id: Optional[strawberry.ID] = ""
    registration_number: Optional[int] = None
    vehicle_owner_id: Optional[strawberry.ID] = ""
    estimator_id: Optional[strawberry.ID] = ""


@strawberry.input
class BaseMaterialModelInput:
    name: Optional[str] = ""
    measure_name: Optional[str] = ""
    quantity: Optional[int] = None
    cost: Optional[float] = None


@strawberry.input
class MaterialInput(BaseMaterialModelInput):
    material_name: Optional[str] = ""


@strawberry.input
class SparePartInput(BaseMaterialModelInput):
    spare_part_name: Optional[str] = ""


@strawberry.input
class WorkInput(BaseMaterialModelInput):
    work_name: Optional[str] = ""
    norma_hour: Optional[float] = None


@strawberry.type
class CarDamageEstimateMutation:
    @strawberry.mutation(
        permission_classes=[PermissionPerModel("car_damage_estimate.add_counteragent")]
    )
    def create_counter_agent(self, input: CounterAgentInput) -> CounterAgent:
        return models.CounterAgent.objects.create(**input.__dict__)

    @strawberry.mutation(
        permission_classes=[
            PermissionPerModel("car_damage_estimate.change_counteragent")
        ]
    )
    def update_counter_agent(
            self, input: CounterAgentInput, id: strawberry.ID
    ) -> CounterAgent:
        models.CounterAgent.objects.filter(id=id).update(**input.__dict__)
        return models.CounterAgent.objects.get(id=id)

    @strawberry.mutation(
        permission_classes=[
            PermissionPerModel("car_damage_estimate.delete_counteragent")
        ]
    )
    def delete_counter_agent(self, id: list[strawberry.ID]) -> int:
        return models.CounterAgent.objects.filter(id__in=id).delete()[0]

    ##################################################################################

    @strawberry.mutation(
        permission_classes=[PermissionPerModel("car_damage_estimate.add_vehicle")]
    )
    def create_vehicle(self, input: VehicleInput) -> Vehicle:
        return models.Vehicle.objects.create(**input.__dict__)

    @strawberry.mutation(
        permission_classes=[PermissionPerModel("car_damage_estimate.change_vehicle")]
    )
    def update_vehicle(self, input: VehicleInput, id: strawberry.ID) -> Vehicle:
        models.Vehicle.objects.filter(id=id).update(**input.__dict__)
        return models.Vehicle.objects.get(id=id)

    @strawberry.mutation(
        permission_classes=[PermissionPerModel("car_damage_estimate.delete_vehicle")]
    )
    def delete_vehicle(self, id: list[strawberry.ID]) -> int:
        return models.Vehicle.objects.filter(id__in=id).delete()[0]

    ###################################################################################

    @strawberry.mutation(
        permission_classes=[PermissionPerModel("car_damage_estimate.add_employee")]
    )
    def create_employee(self, input: EmployeeInput) -> Employee:
        return models.Employee.objects.create(**input.__dict__)

    @strawberry.mutation(
        permission_classes=[PermissionPerModel("car_damage_estimate.change_employee")]
    )
    def update_employee(self, input: EmployeeInput, id: strawberry.ID) -> Employee:
        models.Employee.objects.filter(id=id).update(**input.__dict__)
        return models.Employee.objects.get(id=id)

    @strawberry.mutation(
        permission_classes=[PermissionPerModel("car_damage_estimate.delete_employee")]
    )
    def delete_employee(self, id: list[strawberry.ID]) -> int:
        return models.Employee.objects.filter(id__in=id).delete()[0]

    ###################################################################################

    @strawberry.mutation(
        permission_classes=[
            PermissionPerModel("car_damage_estimate.add_vehicledamageestimate")
        ]
    )
    def create_vehicle_damage_estimate(
            self, input: VehicleDamageEstimateInput
    ) -> VehicleDamageEstimate:
        return models.VehicleDamageEstimate.objects.create(**input.__dict__)

    @strawberry.mutation(
        permission_classes=[
            PermissionPerModel("car_damage_estimate.change_vehicledamageestimate")
        ]
    )
    def update_vehicle_damage_estimate(
            self, input: VehicleDamageEstimateInput, id: strawberry.ID
    ) -> VehicleDamageEstimate:
        models.VehicleDamageEstimate.objects.filter(id=id).update(**input.__dict__)
        return models.VehicleDamageEstimate.objects.get(id=id)

    @strawberry.mutation(
        permission_classes=[
            PermissionPerModel("car_damage_estimate.delete_vehicledamageestimate")
        ]
    )
    def delete_vehicle_damage_estimate(self, id: list[strawberry.ID]) -> int:
        return models.VehicleDamageEstimate.objects.filter(id__in=id).delete()[0]

    ###################################################################################

    @strawberry.mutation(
        permission_classes=[PermissionPerModel("car_damage_estimate.add_material")]
    )
    def create_material(self, input: MaterialInput) -> Material:
        return models.Material.objects.create(**input.__dict__)

    @strawberry.mutation(
        permission_classes=[PermissionPerModel("car_damage_estimate.change_material")]
    )
    def update_material(self, input: MaterialInput, id: strawberry.ID) -> Material:
        models.Material.objects.filter(id=id).update(**input.__dict__)
        return models.Material.objects.get(id=id)

    @strawberry.mutation(
        permission_classes=[PermissionPerModel("car_damage_estimate.delete_material")]
    )
    def delete_material(self, id: list[strawberry.ID]) -> int:
        return models.Material.objects.filter(id__in=id).delete()[0]

    ###################################################################################

    @strawberry.mutation(
        permission_classes=[PermissionPerModel("car_damage_estimate.add_sparepart")]
    )
    def create_spare_part(self, input: SparePartInput) -> SparePart:
        return models.SparePart.objects.create(**input.__dict__)

    @strawberry.mutation(
        permission_classes=[PermissionPerModel("car_damage_estimate.change_sparepart")]
    )
    def update_spare_part(self, input: SparePartInput, id: strawberry.ID) -> SparePart:
        models.SparePart.objects.filter(id=id).update(**input.__dict__)
        return models.SparePart.objects.get(id=id)

    @strawberry.mutation(
        permission_classes=[PermissionPerModel("car_damage_estimate.delete_sparepart")]
    )
    def delete_spare_part(self, id: list[strawberry.ID]) -> int:
        return models.SparePart.objects.filter(id__in=id).delete()[0]

    ###################################################################################

    @strawberry.mutation(
        permission_classes=[PermissionPerModel("car_damage_estimate.add_work")]
    )
    def create_work(self, input: WorkInput) -> Work:
        return models.Work.objects.create(**input.__dict__)

    @strawberry.mutation(
        permission_classes=[PermissionPerModel("car_damage_estimate.change_work")]
    )
    def update_work(self, input: WorkInput, id: strawberry.ID) -> Work:
        models.Work.objects.filter(id=id).update(**input.__dict__)
        return models.Work.objects.get(id=id)

    @strawberry.mutation(
        permission_classes=[PermissionPerModel("car_damage_estimate.delete_work")]
    )
    def delete_work(self, id: list[strawberry.ID]) -> int:
        return models.Work.objects.filter(id__in=id).delete()[0]

    ###################################################################################
