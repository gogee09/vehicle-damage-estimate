from strawberry.types import Info

import strawberry


class CreatePermissionField:
    @classmethod
    def create_permission(cls: "Permissions", permission_name: str, info: Info) -> bool:
        return info.context.request.user.has_perm(permission_name)


@strawberry.type
class Permissions:
    counter_agent_read: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.view_counteragent", info))
    vehicle_read: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.view_vehicle", info))
    employee_read: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.view_employee", info))
    vehicle_damage_estimate_read: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.view_vehicledamageestimate",
                                                                      info))
    material_read: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.view_material", info))
    work_read: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.view_work", info))
    spare_part_read: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.view_sparepart", info))
    counter_agent_create: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.add_counteragent", info))
    counter_agent_update: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.change_counteragent", info))
    counter_agent_delete: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.delete_counteragent", info))
    vehicle_create: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.add_vehicle", info))
    vehicle_update: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.change_vehicle", info))
    vehicle_delete: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.delete_vehicle", info))
    employee_create: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.add_employee", info))
    employee_update: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.change_employee", info))
    employee_delete: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.delete_employee", info))
    vehicle_damage_estimate_create: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.add_vehicledamageestimate",
                                                                      info))
    vehicle_damage_estimate_update: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission(
            "car_damage_estimate.change_vehicledamageestimate", info))
    vehicle_damage_estimate_delete: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission(
            "car_damage_estimate.delete_vehicledamageestimate", info))
    material_create: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.add_material", info))
    material_update: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.change_material", info))
    material_delete: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.delete_material", info))
    work_create: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.add_work", info))
    work_update: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.change_work", info))
    work_delete: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.delete_work", info))
    spare_part_create: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.add_sparepart", info))
    spare_part_update: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.change_sparepart", info))
    spare_part_delete: bool = strawberry.field(
        resolver=lambda info: CreatePermissionField.create_permission("car_damage_estimate.delete_sparepart", info))
