# Generated by Django 3.2.7 on 2021-09-14 10:13

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CounterAgent',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=200, null=True, verbose_name='Name')),
                ('type', models.CharField(blank=True, max_length=200, null=True, verbose_name='Type')),
                ('address', models.CharField(blank=True, max_length=200, null=True, verbose_name='Address')),
                ('phone_number', models.CharField(blank=True, max_length=200, null=True, verbose_name='Phone number')),
                ('payment_account', models.CharField(blank=True, max_length=200, null=True, verbose_name='Payment account')),
            ],
        ),
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('birth_date', models.DateField(blank=True, null=True)),
                ('passport_serial', models.CharField(blank=True, max_length=200, null=True, verbose_name='Passport serial')),
                ('address', models.CharField(blank=True, max_length=200, null=True, verbose_name='Address')),
                ('phone_number', models.CharField(blank=True, max_length=200, null=True, verbose_name='Phone number')),
                ('fms', models.CharField(blank=True, max_length=200, null=True, verbose_name='FMS')),
                ('user', models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Material',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=200, null=True, verbose_name='Name')),
                ('measure_name', models.CharField(blank=True, max_length=200, null=True, verbose_name='Measure name')),
                ('quantity', models.IntegerField(blank=True, null=True)),
                ('cost', models.FloatField(blank=True, null=True)),
                ('material_name', models.CharField(blank=True, max_length=200, null=True, verbose_name='Material name')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='SparePart',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=200, null=True, verbose_name='Name')),
                ('measure_name', models.CharField(blank=True, max_length=200, null=True, verbose_name='Measure name')),
                ('quantity', models.IntegerField(blank=True, null=True)),
                ('cost', models.FloatField(blank=True, null=True)),
                ('spare_part_name', models.CharField(blank=True, max_length=200, null=True, verbose_name='Spare part')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Vehicle',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vehicle_mark', models.CharField(blank=True, max_length=200, null=True, verbose_name='Vehicle mark')),
                ('vehicle_reg_number', models.CharField(blank=True, max_length=200, null=True, verbose_name='Registration number')),
                ('color', models.CharField(blank=True, max_length=200, null=True, verbose_name='Color')),
                ('mileage', models.IntegerField(blank=True, null=True)),
                ('year_of_production', models.DateField(blank=True, null=True)),
                ('serial_and_pts', models.CharField(blank=True, max_length=200, null=True, verbose_name='Serial')),
                ('vin_number', models.CharField(blank=True, max_length=200, null=True, verbose_name='VIN number')),
                ('engine_serial_number', models.IntegerField(blank=True, null=True)),
                ('chassis_serial_number', models.IntegerField(blank=True, null=True)),
                ('owner', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='owner', to='car_damage_estimate.counteragent')),
            ],
        ),
        migrations.CreateModel(
            name='Work',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=200, null=True, verbose_name='Name')),
                ('measure_name', models.CharField(blank=True, max_length=200, null=True, verbose_name='Measure name')),
                ('quantity', models.IntegerField(blank=True, null=True)),
                ('cost', models.FloatField(blank=True, null=True)),
                ('work_name', models.CharField(blank=True, max_length=200, null=True, verbose_name='Work name')),
                ('norma_hour', models.FloatField(blank=True, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='VehicleDamageEstimate',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('estimation_date', models.DateTimeField(null=True)),
                ('case_number', models.CharField(blank=True, max_length=200, null=True, verbose_name='Case number')),
                ('order_number', models.IntegerField(blank=True, null=True)),
                ('registration_number', models.IntegerField(blank=True, null=True)),
                ('estimator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='car_damage_estimate.employee', verbose_name='Employee')),
                ('vehicle_id', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='car_damage_estimate.vehicle', verbose_name='Vehicle')),
                ('vehicle_owner', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='car_damage_estimate.counteragent', verbose_name='Vehicle owner and customer')),
            ],
        ),
    ]
