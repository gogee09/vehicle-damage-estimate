from django.apps import AppConfig


class CarDamageEstimateConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'car_damage_estimate'

    def ready(self):
        import app_root.celery
