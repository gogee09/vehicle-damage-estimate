from typing import Any

from requests_html import HTMLSession
from dateutil.parser import parse
from car_damage_estimate.models import Vehicle, SparePart
from datetime import datetime


def is_date(string, fuzzy=False):
    try:
        parse(string, fuzzy=fuzzy)
        return True

    except ValueError:
        return False


def parse_vehicle_models():
    session = HTMLSession()
    response = session.get(
        'https://www.back4app.com/database/back4app/car-make-model-dataset/all-cars-by-model-and-by-make-and-by-year')

    vehicles = response.html.find('tr')

    list_of_vehicle_models = []

    for vehicle in vehicles:
        list_of_vehicle_models.append(vehicle.text.split("\n"))

    print(list_of_vehicle_models)

    for vehicle_data in list_of_vehicle_models[1: len(list_of_vehicle_models) - 1]:
        try:
            Vehicle.objects.create(vehicle_mark=vehicle_data[0], color=vehicle_data[1], serial_and_pts=vehicle_data[2],
                                   year_of_production=datetime.strptime(vehicle_data[3], "%Y"))
        except IndexError as e:
            print(e)


def parse_spare_parts():
    session = HTMLSession()
    response = session.get(
        'https://www.metabo-service.com/da/spare-parts/?searchTerm=00448')

    spare_parts = response.html.find('tr')

    spare_parts_list = [spare_part.text.split("\n") for spare_part in spare_parts]

    print(spare_parts_list)

    for spare_part_data in spare_parts_list[1: len(spare_parts_list) - 1]:
        SparePart.objects.create(name=spare_part_data[0], measure_name=spare_part_data[1], quantity=spare_part_data[2],
                                 spare_part_name=spare_part_data[3])
