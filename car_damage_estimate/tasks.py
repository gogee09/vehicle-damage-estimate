from car_damage_estimate.scripts.vehicle_parse import parse_vehicle_models, parse_spare_parts
from celery import shared_task


@shared_task
def vehicle_parse_async():
    parse_vehicle_models()


@shared_task
def spare_parts_parse_async():
    parse_spare_parts()
