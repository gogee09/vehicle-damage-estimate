import pytest
import requests
import django

django.setup()

from django.contrib.auth.models import User, Permission
from car_damage_estimate.models import CounterAgent


def test_connection():
    request = requests.get('http://cde.ddns.net/')
    assert request.status_code == 200


def test_is_authenticated():
    user = User.objects.get(username="test")
    assert user.is_authenticated


def test_user_exists():
    assert User.objects.filter(username="test").exists()


def test_user_perms():
    permissions = Permission.objects.all()
    test_user = User.objects.get(username="test")

    for permission in permissions:
        assert test_user.has_perm(f'{permission.content_type.app_label}.{permission.codename}')


@pytest.mark.parametrize("address, name, payment_account, type", [('Pushkin st.', 'Victor', '000123456', 'none')])
def test_create_counteragent(address, name, payment_account, type):
    counter_agent, created = CounterAgent.objects.get_or_create(address=address, name=name,
                                                                payment_account=payment_account, type=type)
    assert created
