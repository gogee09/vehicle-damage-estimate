from django.contrib import admin
from . import models
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User


class EmployeeInline(admin.StackedInline):
    model = models.Employee
    can_delete = False
    verbose_name_plural = "employee"


class UserAdmin(BaseUserAdmin):
    inlines = (EmployeeInline,)


# Register your models here.
admin.site.register(models.Vehicle)
admin.site.register(models.CounterAgent)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(models.VehicleDamageEstimate)
admin.site.register(models.Material)
admin.site.register(models.SparePart)
admin.site.register(models.Work)
