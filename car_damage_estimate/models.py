from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class CounterAgent(models.Model):
    name = models.CharField(max_length=200, verbose_name='Name', null=True, blank=True)
    type = models.CharField(max_length=200, verbose_name='Type', null=True, blank=True)
    address = models.CharField(max_length=200, verbose_name='Address', null=True, blank=True)
    phone_number = models.CharField(max_length=200, verbose_name='Phone number', null=True, blank=True)
    payment_account = models.CharField(max_length=200, verbose_name='Payment account', null=True, blank=True)

    def __str__(self) -> str:
        return f'{self.name}'


class Vehicle(models.Model):
    vehicle_mark = models.CharField(max_length=200, verbose_name='Vehicle mark', null=True, blank=True)
    vehicle_reg_number = models.CharField(max_length=200, verbose_name='Registration number', null=True, blank=True)
    color = models.CharField(max_length=200, verbose_name='Color', null=True, blank=True)
    mileage = models.IntegerField(null=True, blank=True)
    year_of_production = models.DateField(blank=True, null=True)
    serial_and_pts = models.CharField(max_length=200, verbose_name='Serial', blank=True, null=True)
    vin_number = models.CharField(max_length=200, verbose_name='VIN number', null=True, blank=True)
    engine_serial_number = models.IntegerField(null=True, blank=True)
    chassis_serial_number = models.IntegerField(null=True, blank=True)
    owner = models.ForeignKey(CounterAgent, related_name='owner', null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self) -> str:
        return f'{self.vehicle_mark} {self.vehicle_reg_number}'


class Employee(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    birth_date = models.DateField(null=True, blank=True)
    passport_serial = models.CharField(max_length=200, verbose_name='Passport serial', null=True, blank=True)
    address = models.CharField(max_length=200, verbose_name='Address', null=True, blank=True)
    phone_number = models.CharField(max_length=200, verbose_name='Phone number', null=True, blank=True)
    fms = models.CharField(max_length=200, verbose_name='FMS', null=True, blank=True)

    verbose_name = 'Employee'
    verbose_name_plural = 'Employees'

    def __str__(self) -> str:
        return f'{self.user.first_name}'


class VehicleDamageEstimate(models.Model):
    estimation_date = models.DateTimeField(null=True)
    case_number = models.CharField(max_length=200, verbose_name='Case number', null=True, blank=True)
    order_number = models.IntegerField(null=True, blank=True)
    vehicle_id = models.ForeignKey(Vehicle, verbose_name='Vehicle', null=True, blank=True, on_delete=models.SET_NULL)
    registration_number = models.IntegerField(null=True, blank=True)
    vehicle_owner = models.ForeignKey(CounterAgent, verbose_name='Vehicle owner and customer', null=True, blank=True,
                                      on_delete=models.SET_NULL)
    estimator = models.ForeignKey(Employee, verbose_name='Employee', null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self) -> str:
        return f'{self.case_number} {self.order_number}'


class BaseMaterialModel(models.Model):
    name = models.CharField(max_length=200, verbose_name='Name', null=True, blank=True)
    measure_name = models.CharField(max_length=200, verbose_name='Measure name', null=True, blank=True)
    quantity = models.IntegerField(null=True, blank=True)
    cost = models.FloatField(null=True, blank=True)

    class Meta:
        abstract = True


class Material(BaseMaterialModel):
    material_name = models.CharField(max_length=200, verbose_name='Material name', null=True, blank=True)

    verbose_name = 'Material'
    verbose_name_plural = 'Materials'

    def __str__(self) -> str:
        return f'{self.material_name}'


class SparePart(BaseMaterialModel):
    spare_part_name = models.CharField(max_length=200, verbose_name='Spare part', null=True, blank=True)

    verbose_name = 'Spare part'
    verbose_name_plural = 'Spare parts'

    def __str__(self) -> str:
        return f'{self.spare_part_name}'


class Work(BaseMaterialModel):
    work_name = models.CharField(max_length=200, verbose_name='Work name', null=True, blank=True)
    norma_hour = models.FloatField(null=True, blank=True)

    verbose_name = 'Work'
    verbose_name_plural = 'Works'

    def __str__(self) -> str:
        return f'{self.work_name}'
