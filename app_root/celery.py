import os

from celery import Celery

# Set the default Django settings module for the 'celery' program.
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'app_root.settings')

app = Celery('app_root')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django apps.
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print(f'Request: {self.request!r}')


app.conf.beat_schedule = {
    'vehicle-parse-every-seven-hours': {
        'task': 'car_damage_estimate.tasks.vehicle_parse_async',
        'schedule': crontab(hour=7, minute=30),
        'args': (16, 16),
    },
    'spare-parts-parse-every-eight-hours': {
        'task': 'car_damage_estimate.tasks.spare_parts_parse_async',
        'schedule': crontab(hour=8, minute=30),
        'args': (16, 16),
    }
}