FROM arm32v6/python:3.9.7-alpine3.14

WORKDIR ~/untitled1

ENV PYTHONUNBUFFERED 1

RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev libxml2-dev libxslt-dev python3-dev && pip install psycopg2-binary

 RUN pip install --upgrade pip
COPY requirements.txt .
RUN pip install -r requirements.txt && pip install strawberry-django-jwt

COPY ./ ./